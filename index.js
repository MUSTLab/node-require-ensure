'use strict'

const load = require('require-load');
const stack = require('callsite');

const proto = Object.getPrototypeOf(require);

Object.defineProperties(proto, {
    include: {
        writable: false,
        value: function (dependencies) {
            [].concat(dependencies).forEach(require);
        }
    },
    ensure: {
        writable: false,
        value: function (dependencies, callback, errorCallback, chunkName) {
            const callFile = getCallingFile(),
                callModule = require.cache[callFile] || require(callFile),
                options = { file: callFile, cache: false };

            dependencies = [].concat(dependencies);
            
            return Promise.all(dependencies.map((source) => load(source, options)))
                        .then(() => {
                            if (typeof callback === 'function') {
                                return callback((path) => {
                                    let module;
                                    try {
                                        module = callModule.require(path);
                                    } catch(ex) {
                                        module = require(path);
                                    }
                                    return module;
                                });
                            }
                        })
                        .catch((err) => {
                            if (typeof errorCallback === 'function') {
                                errorCallback(err);
                            }   
                        });
        }
    }
});

function getCallingFile () {
    return stack()[2].getFileName();
}